import java.io.Serializable;

public class Assicurazione implements Serializable, Comparable<Assicurazione> {

    // --------------------------------------------------------------

    private static final long serialVersionUID = 1L;
    private String policyID;
    private String stateCode, country;
    private double eq_SiteLimit, hu_SiteLimit, fl_SiteLimit, fr_SiteLimit;
    private double tiv_2011, tiv_2012, eq_siteDeductible, hu_siteDeductible, fl_siteDeductible, fr_siteDeductible;
    private Position position;
    private String line, construction;
    private int pointGranularity;

    // --------------------------------------------------------------

    public Assicurazione(Assicurazione assicurazione) {
        this.policyID = assicurazione.policyID;
        this.stateCode = assicurazione.stateCode;
        this.country = assicurazione.country;
        this.eq_SiteLimit = assicurazione.eq_SiteLimit;
        this.hu_SiteLimit = assicurazione.hu_SiteLimit;
        this.fl_SiteLimit = assicurazione.fl_SiteLimit;
        this.fr_SiteLimit = assicurazione.fr_SiteLimit;
        this.tiv_2011 = assicurazione.tiv_2011;
        this.tiv_2012 = assicurazione.tiv_2012;
        this.eq_siteDeductible = assicurazione.eq_siteDeductible;
        this.hu_siteDeductible = assicurazione.hu_siteDeductible;
        this.fl_siteDeductible = assicurazione.fl_siteDeductible;
        this.fr_siteDeductible = assicurazione.fr_siteDeductible;
        this.position = new Position(assicurazione.position);
        this.line = assicurazione.line;
        this.construction = assicurazione.construction;
        this.pointGranularity = assicurazione.pointGranularity;
    }

    public Assicurazione(String policyID, String stateCode, String country, double eq_SiteLimit, double hu_SiteLimit,
            double fl_SiteLimit, double fr_SiteLimit, double tiv_2011, double tiv_2012, double eq_siteDeductible,
            double hu_siteDeductible, double fl_siteDeductible, double fr_siteDeductible, Position position,
            String line, String construction, int pointGranularity) {
        this.policyID = policyID;
        this.stateCode = stateCode;
        this.country = country;
        this.eq_SiteLimit = eq_SiteLimit;
        this.hu_SiteLimit = hu_SiteLimit;
        this.fl_SiteLimit = fl_SiteLimit;
        this.fr_SiteLimit = fr_SiteLimit;
        this.tiv_2011 = tiv_2011;
        this.tiv_2012 = tiv_2012;
        this.eq_siteDeductible = eq_siteDeductible;
        this.hu_siteDeductible = hu_siteDeductible;
        this.fl_siteDeductible = fl_siteDeductible;
        this.fr_siteDeductible = fr_siteDeductible;
        this.position = new Position(position);
        this.line = line;
        this.construction = construction;
        this.pointGranularity = pointGranularity;
    }

    public Assicurazione(String[] data) {
        this.policyID = data[0];
        this.stateCode = data[1];
        this.country = data[2];
        this.eq_SiteLimit = Double.valueOf(data[3]);
        this.hu_SiteLimit = Double.valueOf(data[4]);
        this.fl_SiteLimit = Double.valueOf(data[5]);
        this.fr_SiteLimit = Double.valueOf(data[6]);
        this.tiv_2011 = Double.valueOf(data[7]);
        this.tiv_2012 = Double.valueOf(data[8]);
        this.eq_siteDeductible = Double.valueOf(data[9]);
        this.hu_siteDeductible = Double.valueOf(data[10]);
        this.fl_siteDeductible = Double.valueOf(data[11]);
        this.fr_siteDeductible = Double.valueOf(data[12]);
        this.position = new Position(Double.valueOf(data[13]), Double.valueOf(data[14]));
        this.line = data[15];
        this.construction = data[16];
        this.pointGranularity = Integer.valueOf(data[17]);
    }

    // --------------------------------------------------------------

    public Assicurazione clone() {
        return new Assicurazione(this);
    }

    // --------------------------------------------------------------

    public String getPolicyID() {
        return this.policyID;
    }

    public void setPolicyID(String policyID) {
        this.policyID = policyID;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getEq_SiteLimit() {
        return this.eq_SiteLimit;
    }

    public void setEq_SiteLimit(double eq_SiteLimit) {
        this.eq_SiteLimit = eq_SiteLimit;
    }

    public double getHu_SiteLimit() {
        return this.hu_SiteLimit;
    }

    public void setHu_SiteLimit(double hu_SiteLimit) {
        this.hu_SiteLimit = hu_SiteLimit;
    }

    public double getFl_SiteLimit() {
        return this.fl_SiteLimit;
    }

    public void setFl_SiteLimit(double fl_SiteLimit) {
        this.fl_SiteLimit = fl_SiteLimit;
    }

    public double getFr_SiteLimit() {
        return this.fr_SiteLimit;
    }

    public void setFr_SiteLimit(double fr_SiteLimit) {
        this.fr_SiteLimit = fr_SiteLimit;
    }

    public double getTiv_2011() {
        return this.tiv_2011;
    }

    public void setTiv_2011(double tiv_2011) {
        this.tiv_2011 = tiv_2011;
    }

    public double getTiv_2012() {
        return this.tiv_2012;
    }

    public void setTiv_2012(double tiv_2012) {
        this.tiv_2012 = tiv_2012;
    }

    public double getEq_siteDeductible() {
        return this.eq_siteDeductible;
    }

    public void setEq_siteDeductible(double eq_siteDeductible) {
        this.eq_siteDeductible = eq_siteDeductible;
    }

    public double getHu_siteDeductible() {
        return this.hu_siteDeductible;
    }

    public void setHu_siteDeductible(double hu_siteDeductible) {
        this.hu_siteDeductible = hu_siteDeductible;
    }

    public double getFl_siteDeductible() {
        return this.fl_siteDeductible;
    }

    public void setFl_siteDeductible(double fl_siteDeductible) {
        this.fl_siteDeductible = fl_siteDeductible;
    }

    public double getFr_siteDeductible() {
        return this.fr_siteDeductible;
    }

    public void setFr_siteDeductible(double fr_siteDeductible) {
        this.fr_siteDeductible = fr_siteDeductible;
    }

    public Position getPosition() {
        return new Position(this.position);
    }

    public void setPosition(Position position) {
        this.position = new Position(position);
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getConstruction() {
        return this.construction;
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public int getPointGranularity() {
        return this.pointGranularity;
    }

    public void setPointGranularity(int pointGranularity) {
        this.pointGranularity = pointGranularity;
    }

    // --------------------------------------------------------------

    @Override
    public String toString() {
        return "{" + getPolicyID() + ", " + getStateCode() + ", " + getCountry() + ", " + getEq_SiteLimit() + ", "
                + getHu_SiteLimit() + ", " + getFl_SiteLimit() + ", " + getFr_SiteLimit() + ", " + getTiv_2011() + ", "
                + getTiv_2012() + ", " + getEq_siteDeductible() + ", " + getHu_siteDeductible() + ", "
                + getFl_siteDeductible() + ", " + getFr_siteDeductible() + ", " + position.getLatitude() + ", "
                + position.getLongitude() + ", " + getLine() + ", " + getConstruction() + ", " + getPointGranularity()
                + "}";
    }

    // --------------------------------------------------------------   

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Assicurazione ? (policyID.matches(((Assicurazione)obj).policyID)) : false;
    }

    // --------------------------------------------------------------

    @Override
    public int compareTo(Assicurazione o) {
        return policyID.compareTo(o.policyID);
    }
}
