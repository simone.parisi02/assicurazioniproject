import java.util.*;

public class Contatore extends GestoreAssicurazioni {
    // --------------------------------------------------------------

    public Contatore() {
        super();
    }

    public Contatore(ArrayList<Assicurazione> assicurazioni){
        super(assicurazioni);
    }

    public Contatore(Contatore contatore) {
        super(contatore);
    }

    // --------------------------------------------------------------

    public HashMap<String, HashMap<String, Integer>> generateInfo(){
        HashMap<String, HashMap<String, Integer>> dict = new HashMap<String, HashMap<String, Integer>>();
        
        
        // Generazione HashMap --------------------------------------------------------------
        ArrayList<String> constructors = new ArrayList<>();
        ArrayList<String> types = new ArrayList<>();
        for(Assicurazione a : listAssicurazioni){
            boolean findConstructor = false;
            boolean findTypes = false;
            
            for(String s : constructors)
                if(s.matches(a.getConstruction()))
                    findConstructor = true;
            
            for(String s : types)
                if(s.matches(a.getLine()))
                    findTypes = true;
           
            if(!findConstructor)
                constructors.add(a.getConstruction());
            
            if(!findTypes)
                types.add(a.getLine());
        }

        for(String s : constructors){
            dict.put(s, new HashMap<String, Integer>());   
        }

        HashMap<String, Integer> tmpHashMap = new HashMap<String, Integer>();
        for(String a : types){
            tmpHashMap.put(a, 0);
        }

        for(String s : dict.keySet()){
            dict.put(s, new HashMap<>(tmpHashMap)); 
        }
        // Fine Generazione Hash Map -------------------------------------------------

        // Compilazione HashMap-------------------------------------------------------  
        for(Assicurazione a : listAssicurazioni){         
                Integer tmpInteger = dict.get(a.getConstruction()).get(a.getLine()) + 1;
                dict.get(a.getConstruction()).replace(a.getLine(), tmpInteger);
        }

        return dict;
    }
}
