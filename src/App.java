public class App {
    public static void main(String[] args) throws Exception {
        // --------------------------------------------------------------
        Contatore gestoreAssicurazioni = new Contatore();
        // --------------------------------------------------------------
        gestoreAssicurazioni.loadCSV("data.csv", 1000);
        String toPrint = gestoreAssicurazioni.generateInfo().toString();
        // --------------------------------------------------------------
        System.out.println(System.lineSeparator() + toPrint);
        // --------------------------------------------------------------
    }
}
