import java.io.Serializable;

public class Position implements Serializable {
    // --------------------------------------------------------------

    private static final long serialVersionUID = 1L;
    private double latitude;
    private double longitude;

    // --------------------------------------------------------------

    public Position(Position position) {
        latitude = position.latitude;
        longitude = position.longitude;
    }

    public Position(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // --------------------------------------------------------------

    public Position clone() {
        return new Position(this);
    }

    // --------------------------------------------------------------

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    // --------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Position)) {
            return false;
        }
        Position position = (Position) o;
        return latitude == position.latitude && longitude == position.longitude;
    }

    // --------------------------------------------------------------

    @Override
    public String toString() {
        return "{" + " latitude='" + getLatitude() + "'" + ", longitude='" + getLongitude() + "'" + "}";
    }
}
