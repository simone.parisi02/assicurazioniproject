import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class GestoreAssicurazioni {

    // --------------------------------------------------------------

    ArrayList<Assicurazione> listAssicurazioni = new ArrayList<>();

    // --------------------------------------------------------------

    public GestoreAssicurazioni() {
    }

    public GestoreAssicurazioni(ArrayList<Assicurazione> assicurazioni){
        addAll(assicurazioni);
    }

    public GestoreAssicurazioni(GestoreAssicurazioni gestoreAssicurazioni) {
        addAll(gestoreAssicurazioni.listAssicurazioni);
    }

    // --------------------------------------------------------------

    public GestoreAssicurazioni add(Assicurazione assicurazione) {
        listAssicurazioni.add(new Assicurazione(assicurazione));
        return this;
    }

    public GestoreAssicurazioni addAll(ArrayList<Assicurazione> assicurazioni) {
        assicurazioni.forEach((assicurazione) -> add(assicurazione));
        return this;
    }

    public GestoreAssicurazioni add(String[] data) {
        listAssicurazioni.add(new Assicurazione(data));
        return this;
    }

    // --------------------------------------------------------------

    public GestoreAssicurazioni loadCSV(String fileName, int maxLine) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String tmp;
        int counter = 0;
        reader.readLine();
        while ((tmp = reader.readLine()) != null && counter <= maxLine) {
            String[] data = tmp.split(",");
            add(data);
            counter++;
        }
        reader.close();
        return this;
    }

    public GestoreAssicurazioni loadCSV(String name) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(name));
        String tmp;
        while ((tmp = reader.readLine()) != null) {
            add(tmp.split(";"));
        }
        reader.close();
        return this;
    }

    // --------------------------------------------------------------

    public GestoreAssicurazioni saveBin() throws FileNotFoundException, IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.bin"));
        out.writeObject(listAssicurazioni);
        out.close();
        return this;
    }

    @SuppressWarnings("unchecked")
    public GestoreAssicurazioni loadBin(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        listAssicurazioni = (ArrayList<Assicurazione>) in.readObject();
        in.close();
        return this;
    }

    // --------------------------------------------------------------

    @Override
    public String toString() {
        String tmp = "";
        for (Assicurazione assicurazione : listAssicurazioni) {
            tmp = tmp.concat((assicurazione.toString() + System.lineSeparator()));
        }
        return tmp;
    }

    // --------------------------------------------------------------

    public GestoreAssicurazioni remove(Assicurazione assicurazione) {
        listAssicurazioni.remove(assicurazione);
        return this;
    }

    public Assicurazione edit(int index) {
        return index < listAssicurazioni.size() ? listAssicurazioni.get(index) : null;
    }

    public Assicurazione get(int index) {
        return index < listAssicurazioni.size() ? new Assicurazione(listAssicurazioni.get(index)) : null;
    }

    // --------------------------------------------------------------

    public GestoreAssicurazioni sort() {
        Collections.sort(listAssicurazioni);
        return this;
    }
}
